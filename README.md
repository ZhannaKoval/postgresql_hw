# Postgresql_HW
1. Go to Postgresql website, download and install it. Enter password and remember it.
2. Open automatically installed pgAdmin and enter this password again in order to have access to the database. You do not need to create any additional servers.
3. Create database with the name corp.
4. Go to the employees table (provided), download it, then turn it into .csv file by selecting CSV format during SAVE AS procedure. Then go to pgAdmin, create empty employees table and use COPY command to import all the data to your employees table from the original csv table.
5. Normilize it: first you need to remove the SKILLS  column as it violates the 1NF rules. Create SKILLS table and tranfser all the information to it. Then link it with the original table using foreign key and separate table with the id of employee and id of skill.
After that go back to employees table and remove the departments and parent departments columns. Create separate table for departments as they do not depend directly on the primary key (id of employees), add parent_dep_id there. Link them  with the original table using foreign keys.
6. Create stored procedure using CREATE OR REPLACE PROCEDURE command and print all the information needed: enter parent_id and receive the information about employees (only active ones) of this parent department (and all its subdepartments). Also receive the information about avarage salary of this parent department(and all its subdepartments) and the number of its employees (only active ones).
7. Export all the tables you have using IMPORT/EXPORT command of the drop down menu and save your stored procedure using save button on the upper panel (.sql).
8. Push all these files to your new git repository.



