PGDMP     (                    y           corp    13.2    13.2     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16394    corp    DATABASE     a   CREATE DATABASE corp WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Russian_Russia.1251';
    DROP DATABASE corp;
                postgres    false            �            1255    24582    print_employees(integer) 	   PROCEDURE     \  CREATE PROCEDURE public.print_employees(pid integer)
    LANGUAGE plpgsql
    AS $$
DECLARE 
	e_id employees.emp_id%TYPE;
	e_name employees.emp_first_name%TYPE;
	e_surname employees.emp_last_name%TYPE;
	e_salary employees.emp_salary%TYPE;
	e_title titles.title_name%TYPE;
	e_years_in_company character varying;
	e_status employees.id_status%TYPE;
	e_dep employees.id_emp_dep%TYPE;
	e_num integer;
	e_sal_avg integer;
	curs1 cursor for 
	select emp_id, emp_first_name, emp_last_name, emp_salary, title_name, age( current_date, emp_hiring_date), id_status, id_emp_dep 
	from employees join titles on titles.title_id = employees.id_titels 
	where id_emp_dep in (select dep_id from deps where parent_dep_id = pid) and employees.id_status=1;
	curs2 cursor for 
	SELECT count(emp_id), avg(emp_salary) 
	from employees 
	where id_emp_dep in (select dep_id from deps where parent_dep_id = pid) and employees.id_status=1;
BEGIN
	OPEN curs1;
	LOOP 
		FETCH curs1 into e_id, e_name, e_surname, e_salary, e_title, e_years_in_company;
		IF NOT FOUND THEN
			EXIT;
		END IF;
		RAISE NOTICE 'EMP: %, %, %, %, %, %', e_id, e_name, e_surname, e_salary, e_title, e_years_in_company;
	END LOOP;
	CLOSE curs1;
	OPEN curs2;
	LOOP 
		FETCH curs2 into e_num, e_sal_avg;
		IF NOT FOUND THEN
			EXIT;
		END IF;
		RAISE NOTICE 'NUMandSal: %, %', e_num, e_sal_avg;
	END LOOP;
	CLOSE curs2;
END;
$$;
 4   DROP PROCEDURE public.print_employees(pid integer);
       public          postgres    false            �            1259    16480    deps    TABLE     u   CREATE TABLE public.deps (
    dep_id integer NOT NULL,
    dep_name character varying,
    parent_dep_id integer
);
    DROP TABLE public.deps;
       public         heap    postgres    false            �            1259    16429 
   emp_skills    TABLE     c   CREATE TABLE public.emp_skills (
    emp_id integer NOT NULL,
    emp_skill_id integer NOT NULL
);
    DROP TABLE public.emp_skills;
       public         heap    postgres    false            �            1259    16576 
   emp_status    TABLE     n   CREATE TABLE public.emp_status (
    emp_status_id integer NOT NULL,
    emp_status_name character varying
);
    DROP TABLE public.emp_status;
       public         heap    postgres    false            �            1259    16413 	   employees    TABLE     4  CREATE TABLE public.employees (
    emp_id integer NOT NULL,
    emp_first_name character varying(20) NOT NULL,
    emp_last_name character varying(20) NOT NULL,
    emp_hiring_date date,
    emp_salary integer,
    emp_exit_date date,
    id_titels integer,
    id_status integer,
    id_emp_dep integer
);
    DROP TABLE public.employees;
       public         heap    postgres    false            �            1259    16421    skills    TABLE     h   CREATE TABLE public.skills (
    emp_skill_id integer NOT NULL,
    emp_skill_name character varying
);
    DROP TABLE public.skills;
       public         heap    postgres    false            �            1259    16444    titles    TABLE     `   CREATE TABLE public.titles (
    title_id integer NOT NULL,
    title_name character varying
);
    DROP TABLE public.titles;
       public         heap    postgres    false            �          0    16480    deps 
   TABLE DATA           ?   COPY public.deps (dep_id, dep_name, parent_dep_id) FROM stdin;
    public          postgres    false    204   �'       �          0    16429 
   emp_skills 
   TABLE DATA           :   COPY public.emp_skills (emp_id, emp_skill_id) FROM stdin;
    public          postgres    false    202   (       �          0    16576 
   emp_status 
   TABLE DATA           D   COPY public.emp_status (emp_status_id, emp_status_name) FROM stdin;
    public          postgres    false    205   �(       �          0    16413 	   employees 
   TABLE DATA           �   COPY public.employees (emp_id, emp_first_name, emp_last_name, emp_hiring_date, emp_salary, emp_exit_date, id_titels, id_status, id_emp_dep) FROM stdin;
    public          postgres    false    200   �(       �          0    16421    skills 
   TABLE DATA           >   COPY public.skills (emp_skill_id, emp_skill_name) FROM stdin;
    public          postgres    false    201   �*       �          0    16444    titles 
   TABLE DATA           6   COPY public.titles (title_id, title_name) FROM stdin;
    public          postgres    false    203   W+       E           2606    16497    deps deps_details_pk 
   CONSTRAINT     V   ALTER TABLE ONLY public.deps
    ADD CONSTRAINT deps_details_pk PRIMARY KEY (dep_id);
 >   ALTER TABLE ONLY public.deps DROP CONSTRAINT deps_details_pk;
       public            postgres    false    204            A           2606    16433    emp_skills emp_skills_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.emp_skills
    ADD CONSTRAINT emp_skills_pkey PRIMARY KEY (emp_id, emp_skill_id);
 D   ALTER TABLE ONLY public.emp_skills DROP CONSTRAINT emp_skills_pkey;
       public            postgres    false    202    202            G           2606    16583    emp_status emp_status_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.emp_status
    ADD CONSTRAINT emp_status_pkey PRIMARY KEY (emp_status_id);
 D   ALTER TABLE ONLY public.emp_status DROP CONSTRAINT emp_status_pkey;
       public            postgres    false    205            ;           2606    16420    employees employees_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.employees
    ADD CONSTRAINT employees_pkey PRIMARY KEY (emp_id);
 B   ALTER TABLE ONLY public.employees DROP CONSTRAINT employees_pkey;
       public            postgres    false    200            ?           2606    16428    skills skills_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.skills
    ADD CONSTRAINT skills_pkey PRIMARY KEY (emp_skill_id);
 <   ALTER TABLE ONLY public.skills DROP CONSTRAINT skills_pkey;
       public            postgres    false    201            C           2606    16451    titles titles_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.titles
    ADD CONSTRAINT titles_pkey PRIMARY KEY (title_id);
 <   ALTER TABLE ONLY public.titles DROP CONSTRAINT titles_pkey;
       public            postgres    false    203            <           1259    24581    fki_deps_constr    INDEX     K   CREATE INDEX fki_deps_constr ON public.employees USING btree (id_emp_dep);
 #   DROP INDEX public.fki_deps_constr;
       public            postgres    false    200            =           1259    16590    fki_status_fk_constr    INDEX     O   CREATE INDEX fki_status_fk_constr ON public.employees USING btree (id_status);
 (   DROP INDEX public.fki_status_fk_constr;
       public            postgres    false    200            J           2606    24576    employees deps_constr    FK CONSTRAINT     �   ALTER TABLE ONLY public.employees
    ADD CONSTRAINT deps_constr FOREIGN KEY (id_emp_dep) REFERENCES public.deps(dep_id) NOT VALID;
 ?   ALTER TABLE ONLY public.employees DROP CONSTRAINT deps_constr;
       public          postgres    false    200    2885    204            K           2606    16452    skills emp_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.skills
    ADD CONSTRAINT emp_id FOREIGN KEY (emp_skill_id) REFERENCES public.employees(emp_id) NOT VALID;
 7   ALTER TABLE ONLY public.skills DROP CONSTRAINT emp_id;
       public          postgres    false    2875    201    200            M           2606    16439    emp_skills skill_fk_constr    FK CONSTRAINT     �   ALTER TABLE ONLY public.emp_skills
    ADD CONSTRAINT skill_fk_constr FOREIGN KEY (emp_skill_id) REFERENCES public.skills(emp_skill_id);
 D   ALTER TABLE ONLY public.emp_skills DROP CONSTRAINT skill_fk_constr;
       public          postgres    false    202    201    2879            L           2606    16434    emp_skills skills_fk_constr    FK CONSTRAINT     �   ALTER TABLE ONLY public.emp_skills
    ADD CONSTRAINT skills_fk_constr FOREIGN KEY (emp_id) REFERENCES public.employees(emp_id);
 E   ALTER TABLE ONLY public.emp_skills DROP CONSTRAINT skills_fk_constr;
       public          postgres    false    200    202    2875            I           2606    16585    employees status_fk_constr    FK CONSTRAINT     �   ALTER TABLE ONLY public.employees
    ADD CONSTRAINT status_fk_constr FOREIGN KEY (id_status) REFERENCES public.emp_status(emp_status_id) NOT VALID;
 D   ALTER TABLE ONLY public.employees DROP CONSTRAINT status_fk_constr;
       public          postgres    false    2887    205    200            H           2606    16570    employees titlesfk    FK CONSTRAINT     z   ALTER TABLE ONLY public.employees
    ADD CONSTRAINT titlesfk FOREIGN KEY (id_titels) REFERENCES public.titles(title_id);
 <   ALTER TABLE ONLY public.employees DROP CONSTRAINT titlesfk;
       public          postgres    false    200    203    2883            �   u   x�-�1�0@��>�'F�B3�R���r\A�*1��L_��.a��g��<�'�>�ў�-P���v@�G�	hԢ�e!�@A��N۪���h���U���ʁ�y��0�q�#�v#�      �   m   x�%��1��t0[��s���X�<Z�\�qs�氩�$KYVs�жC�\x�?��O<�k����C�����,�|U�Hqۇ�����{�{Y�k�k���O�?�      �   ,   x�3�LL.�,K�2��̃2�9sKR��2K*rR�"1z\\\ /�Q      �   �  x�M�ˎ�0�����|M��LA��(#��f6nc�V.FN*Oϱs�HU��d���%���~-W;?�!�������j�9����
�l�^�����DU�7�&�iM�0Qco����!^{;$��i���7�5Sx���ϩC��Ȳ�u�9�
a� ���K�8���L�)#_9��s8�gh�UI9JH�_�;�x��p��z�j)�sN�o6�~���Rq�X��QސS��ޟB��o��+��`�*���6������g%�������h�2�ن&ػ����vv2Mq�!x����+P��b���p���r�ߏ��ع!�LU*S.��e�«mz���7����4H��2�+��8�-fR�Y�5��C�~;�.�j���6>֠D�`5�����Fl�'��PZ.;<[�����֧���];�qN���)�ŮR˚�D�������ԱU      �   �   x�M�Kr1D�p
N��b����k�CYB*�L*���x�MW7�T��*U,��O2���(���A�ղ�����
�9��B�o@Cz�x߃u6��?�B�<^�꣝�T�V�-����d9uǔ���cuy�j�1�UiE�v�_B�,RZ�����$���M�je��h&�·m�������Ue      �   �   x�m��
1���S�	���qY��Tѣ�Z�R�M��"��A���$��띃���`�h,{K��g�RF/�x.�2xJ��𙐸�L�#����V�8���m�͟]�<V(w
�w��t<�B��fkU�j�*�4�;�����ŀ�:_bb�� ^��J�     